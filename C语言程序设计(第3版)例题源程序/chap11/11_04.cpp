#if 1
void main( )
{   int i;
    int a[5] = {6, 5, 2, 8, 1};

   void fsort(int a[ ], int n);
    fsort(a, 5);
    for(i = 0; i < 5; i++)
        printf("%d ", a[i]);
 }

void fsort(int a[ ], int n)
{     int k, j;
      int temp;
      for(k = 1; k < n; k++)
         for(j = 0; j < n-k; j++)
             if(a[j] > a[j+1]){
                  temp = a[j];
                  a[j] = a[j+1];
                  a[j+1] = temp;
            }
}
#endif // 1

#if 0

/*【例11-4】将5个字符串从小到大排序后输出。*/

#include <stdio.h>
#include <string.h>
int main(void )
{
    int i;
    char *pcolor[ ] = {"red", "blue", "yellow",
 "green", "black"};
    void fsort(char *color[ ], int n);

    fsort(pcolor, 5);   /* 调用函数 */
    for(i = 0; i < 5; i++)
        printf("%s ", pcolor[i]);

    return 0;
}

void fsort(char *color[ ], int n)
{
    int k, j;
    char *temp;
    for(k = 1; k < n; k++)
      for(j = 0; j < n-k; j++)
        if(strcmp(color[j], color[j+1]) > 0){
          temp = color[j];
          color[j] = color[j+1];
          color[j+1] = temp;
        }
}


#endif // 1