/*【例11-6】解密藏头诗。所谓藏头诗，就是将一首诗每一句的第一个字连起来，所组成的内容就是该诗的真正含义。编写程序，输出一首藏头诗的真实含义。*/

/* 解密藏头诗，指针数组操作 */
#include <stdio.h>
char *change(char s[][20]);
int main(void)
{
   int i;
   char *poem[4] = { "一叶轻舟向东流，", "帆梢轻握杨柳手，", "风纤碧波微起舞，", "顺水任从雅客悠。"};    /* 指针数组初始化 */
   char mean[10];
   for(i = 0; i < 4; i++){  /* 每行取第1个汉字存入mean */
      mean[2 * i] = *(poem[i]);
      mean[2 * i + 1] = *(poem[i] + 1);
   }
   mean[2 * i] = '\0';
   printf("%s\n", mean);    /* 输出结果 */

   return 0;
}
